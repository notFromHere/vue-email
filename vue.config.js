const path = require( 'path' );

const resolve = ( dir ) => {
  return path.join( __dirname, dir );
};

const sassResources = '@import "@/assets/styles/style.sass"';

module.exports = {
  configureWebpack: {
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@$': resolve( 'src' )
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: sassResources
      }
    }
  },
};
