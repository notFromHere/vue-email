/* Helpers file */
import { xor } from 'lodash';
import Qs from 'qs';

/**
 * name: hDeepCopy
 *
 * creates deep copy of object (HACK)
 *
 * @param {Object} object
 * @return {Object}
 */
export const hDeepCopy = ( object ) => { return JSON.parse( JSON.stringify( object ) ); };

/**
 * name: hToggleArrayItem
 *
 * Creates an array of unique values that is the symmetric difference of the given arrays.
 * The order of result values is determined by the order they occur in the arrays.
 *
 * @param {Array} array
 * @param {any} item
 * @returns {Array} newArray
 */
export const hToggleArrayItem = ( array, item ) => { return xor( array, [item] ); };

export const hObjectIsEmpty = ( obj ) => { return Object.keys( obj ).length === 0; };

export const hMapColectionAsFilterObject = ( collection, { asLabel, asValue } ) => {
  return collection.map( ( item ) => {
    return {
      label: item[asLabel],
      value: item[asValue]
    };
  } );
};

export const hParseToFilterCriteriaObjectRequiredByApi = ( filterCriteria ) => {
  const parsedFilterCriteria = {};

  Object.keys( filterCriteria )
    .forEach( ( item ) => {
      parsedFilterCriteria[item] = filterCriteria[item].value;
    } );

  return parsedFilterCriteria;
};

export const hCalculateSummaryOfProducts = ( productsList ) => {
  if ( productsList.length <= 1 ) {
    return {
      quantity: productsList[0] ? productsList[0].quantity : 0,
      amount: productsList[0] ? productsList[0].amount : 0
    };
  }

  const summaryQuantity = [];
  const summaryAmount = [];

  productsList.forEach( ( item ) => {
    summaryQuantity.push( item.quantity );
    summaryAmount.push( item.quantity * item.amount );
  } );

  return {
    quantity: summaryQuantity.reduce( ( prev, next ) => { return prev + next; }, 0 ),
    amount: summaryAmount.reduce( ( prev, next ) => { return prev + next; }, 0 )
  };
};

export const hDeepClone = ( obj ) => { return JSON.parse( JSON.stringify( obj ) ); };

export function hBasename( path, suffix ) {
  let b = path;
  let lastChar = b.charAt( b.length - 1 );

  if ( lastChar === '/' || lastChar === '\\' ) {
    b = b.slice( 0, -1 );
  }

  b = b.replace( /^.*[/\\]/g, '' );

  if ( typeof suffix === 'string' && b.substr( b.length - suffix.length ) === suffix ) {
    b = b.substr( 0, b.length - suffix.length );
  }

  return b;
}

export function hGetPathInfo( path, options ) {
  let optionsCurrent = options;
  let realOpt = '';
  let optTemp = 0;
  let tmpArr = {};
  let cnt = 0;
  let i = 0;
  let haveBasename = false;
  let haveExtension = false;
  let haveFilename = false;

  if ( !path ) {
    return false;
  }
  if ( !options ) {
    optionsCurrent = 'PATHINFO_ALL';
  }

  // Initialize binary arguments. Both the string & integer (constant) input is
  // allowed
  let OPTS = {
    PATHINFO_DIRNAME: 1,
    PATHINFO_BASENAME: 2,
    PATHINFO_EXTENSION: 4,
    PATHINFO_FILENAME: 8,
    PATHINFO_ALL: 0
  };
  // PATHINFO_ALL sums up all previously defined PATHINFOs (could just pre-calculate)
  Object.keys( OPTS )
    .forEach( ( key ) => {
      if ( OPTS[key] ) {
        OPTS.PATHINFO_ALL = OPTS[key];
      }
    } );
  if ( typeof optionsCurrent !== 'number' ) {
    // Allow for a single string or an array of string flags
    optionsCurrent = [].concat( optionsCurrent );
    for ( i = 0; i < options.length; i += 1 ) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if ( OPTS[optionsCurrent[i]] ) {
        optTemp = OPTS[optionsCurrent[i]];
      }
    }
    optionsCurrent = optTemp;
  }

  // Internal Functions
  let _getExt = ( pathItem ) => {
    let str = `${pathItem}`;
    let dotP = str.lastIndexOf( '.' ) + 1;
    let strCurrent = dotP !== str.length ? str.substr( dotP ) : '';

    return !dotP ? false : strCurrent;
  };

  // Gather path infos
  if ( options && OPTS.PATHINFO_DIRNAME ) {
    let dirName = path
      .replace( /\\/g, '/' )
      .replace( /\/[^/]*\/?$/, '' ); // dirname
    tmpArr.dirname = dirName === path ? '.' : dirName;
  }

  if ( options && OPTS.PATHINFO_BASENAME ) {
    if ( haveBasename === false ) {
      haveBasename = hBasename( path );
    }
    tmpArr.basename = haveBasename;
  }

  if ( options && OPTS.PATHINFO_EXTENSION ) {
    if ( haveBasename === false ) {
      haveBasename = hBasename( path );
    }
    if ( haveExtension === false ) {
      haveExtension = _getExt( haveBasename );
    }
    if ( haveExtension !== false ) {
      tmpArr.extension = haveExtension;
    }
  }

  if ( options && OPTS.PATHINFO_FILENAME ) {
    if ( haveBasename === false ) {
      haveBasename = hBasename( path );
    }
    if ( haveExtension === false ) {
      haveExtension = _getExt( haveBasename );
    }
    if ( haveFilename === false ) {
      haveExtension = haveExtension ? haveExtension.length + 1 : ( haveExtension );
      haveExtension = haveExtension === false ? 0 : 1;

      haveFilename = haveBasename.slice( 0, haveBasename.length - ( haveExtension ) );
    }

    tmpArr.filename = haveFilename;
  }

  // If array contains only 1 element: return string
  cnt = 0;
  Object.keys( tmpArr )
    .forEach( ( optItem ) => {
      if ( tmpArr[optItem] ) {
        cnt += 1;
        realOpt = optItem;
      }
    } );
  if ( cnt === 1 ) {
    return tmpArr[realOpt];
  }

  // Return full-blown array
  return tmpArr;
}

export function hConvertImageToBase64( file = {}, callback = () => {
} ) {
  const reader = new FileReader();
  reader.addEventListener( 'load', () => { return callback( reader.result ); } );
  reader.readAsDataURL( file );
}

export function hSerializeHttpParams( httpParams ) {
  const serializedParams = Qs.stringify( httpParams, {
    arrayFormat: 'brackets',
    encode: false
  } );

  // add serialized empty params
  const serializedEmptyParams = Object.keys( httpParams )
    .filter( ( param ) => { return typeof httpParams[param] === 'object' && hObjectIsEmpty( httpParams[param] ); } )
    .map( ( param ) => { return `${param}[]`; } )
    .join( '&' );

  const _emptyParams = serializedEmptyParams.length === 0 ? '' : `&${serializedEmptyParams}`;
  return `${serializedParams}${_emptyParams}`;
}

export function hFileExtension( fileName ) {
  return fileName
    .split( '.' )
    .pop();
}
