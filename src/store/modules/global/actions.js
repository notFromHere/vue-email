// Vuex Client module actions
import axios from 'axios';
import {
  FILL_DETAILS
} from './mutations-types';

export default {
  async getDetails( { commit }, client ) {
    try {
      const params = { client };
      const { record, total } = ( await axios.get( 'sth/getDetails', { params } ) );
      commit( FILL_DETAILS, {
        record,
        total
      } );
    } catch ( e ) {
      console.log( e );
    }
  }
};
