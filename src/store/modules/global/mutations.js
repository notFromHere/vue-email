// Vuex Client module mutations
import {
  FILL_DETAILS
} from './mutations-types';

export default {
  [FILL_DETAILS]( state, payload ) {
    const _state = state;
    _state.details.record = payload.record;
    _state.details.total = payload.total;
  }
};
