// Vuex Client module main file
import actions from './actions';
import mutations from './mutations';

const INITIAL_STATE = {
  details: {
    record: {},
    total: 0
  },
};

const getters = {
  details: ( state ) => {
    return state.details.record;
  }
};

// VUEX MODULE /////////////////////////////////////////////////////////////////
const Module = {
  namespaced: true,
  state: INITIAL_STATE,
  mutations,
  actions,
  getters,
  modules: {}
};

export default Module;
