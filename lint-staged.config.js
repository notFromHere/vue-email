const path = require( 'path' );

const resolve = ( dir ) => { return path.join( __dirname, dir ); };

module.exports = {
  linters: {
    'src/**/*.{sass,vue}': ['stylelint --fix --custom-syntax postcss-html postcss-sass', 'git add'],
    'src/**/*.{js,vue}': ['eslint --fix', 'git add']
  },
  ignore: ['node_modules', 'dist', 'yarn.lock'],
  /**
   * HACK:
   * Jeżeli lint-staged jest uruchmiany z husky lub z lerna, domyślna ścieżka, w której
   * szuka folderu .git to ścieżka, z której jest uruchamiany (a nie root, gdzie faktycznie znajduje się .git).
   * To powoduje błąd: 'No .git repository'. W poprzedniej wersji lint-staged można było w konfiguracji wskazać
   * domyślną ścieżkę dla git ('gitDir') ale ta opcja już jest 'depraceted'.
   * Po wielu godzinach spędzonych w grzebaniu w kodzie źródłowym lint-staged, znalazłem rozwiązanie.
   * W pliku src/execGit.js w funkcji, która uruchamia git'a aby sprawdzić ścieżkę do repozytorium znalazłem, że:
   * ...
   * const cwd = options.cwd || process.cwd()
   * ...
   * Nie wiem, czy to jest celowe, bo w dokumentacji nie ma informacji o opcji 'cwd' ale to rozwiązało problem.
   * Opcja 'cwd' nie przechodzi walidacji konfiguracji w lint-staged ale nie sypie błędem tylko ostrzeżeniem i w dodatku
   * jeżeli uruchomi się z flagą '--debug'.
   * @sebastian
   */
  cwd: resolve( '../' )
};
